
from rest_framework.views import APIView
from rest_framework.decorators import api_view, schema
from rest_framework import status
from rest_framework.response import Response
import json
import requests
import configparser
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

config = configparser.ConfigParser()
config.read(os.path.join(BASE_DIR, '../config.ini'))

SOUTH_AMERICAN_COUNTRIES_LIST = [
    {
        "name": "Argentina",
        "capital": "Buenos Aires",
        "id": 3435910,
        "country": "AR",
        "coord": {
            "lon": -58.377232,
            "lat": -34.613152
        }
    },
    {
        "name": "Bolivia (Plurinational State of)",
        "capital": "Sucre",
        "id": 3903987,
        "country": "BO",
        "coord": {
            "lon": -65.262741,
            "lat": -19.033319
        }
    },
    {
        "name": "Brazil",
        "capital": "Brasília",
        "id": 3410315,
        "country": "BR",
        "coord": {
            "lon": -42.694172,
            "lat": -3.89472
        }
    },
    {
        "name": "Chile",
        "capital": "Santiago",
        "id": 3871336,
        "country": "CL",
        "coord": {
            "lon": -70.64827,
            "lat": -33.45694
        }
    },
    {
        "name": "Colombia",
        "capital": "Bogotá",
        "id": 3688689,
        "country": "CO",
        "coord": {
            "lon": -74.081749,
            "lat": 4.60971
        }
    },
    {
        "name": "Ecuador",
        "capital": "Quito",
        "id": 3652462,
        "country": "EC",
        "coord": {
            "lon": -78.524948,
            "lat": -0.22985
        }
    },
    {
        "name": "Falkland Islands (Malvinas)",
        "id": 3474414,
        "capital": "Stanley",
        "country": "FK",
        "coord": {
            "lon": -59.166672,
            "lat": -51.75
        }
    },
    {
        "name": "French Guiana",
        "capital": "Cayenne",
        "id": 6690689,
        "country": "GF",
        "coord": {
            "lon": -52.334999,
            "lat": 4.9386
        }
    },
    {
        "name": "Guyana",
        "capital": "Georgetown",
        "id": 3378644,
        "country": "GY",
        "coord": {
            "lon": -58.15527,
            "lat": 6.80448
        }
    },
    {
        "name": "Paraguay",
        "capital": "Asunción",
        "id": 3474570,
        "country": "PY",
        "coord": {
            "lon": -57.666672,
            "lat": -25.26667
        }
    },
    {
        "name": "Peru",
        "id": 3936456,
        "capital": "Lima",
        "country": "PE",
        "coord": {
            "lon": -77.028236,
            "lat": -12.04318
        }
    },
    {
        "name": "South Georgia and the South Sandwich Islands",
        "capital": "King Edward Point",
        "id": 3474415,
        "country": "GS",
        "coord": {
            "lon": -33,
            "lat": -56
        }
    },
    {
        "name": "Suriname",
        "id": 3383330,
        "capital": "Paramaribo",
        "country": "SR",
        "coord": {
            "lon": -55.166821,
            "lat": 5.86638
        }
    },
    {
        "name": "Uruguay",
        "id": 3441575,
        "capital": "Montevideo",
        "country": "UY",
        "coord": {
            "lon": -56.167351,
            "lat": -34.833462
        }
    },
    {
        "name": "Venezuela (Bolivarian Republic of)",
        "capital": "Caracas",
        "id": 3646738,
        "country": "VE",
        "coord": {
            "lon": -66.879189,
            "lat": 10.48801
        }
    }
]


class Weather(APIView):
    def get(self, request, *args, **kwargs):
        url = config["OPENWEATHER"]["forecast"]
        key = config["OPENWEATHER"]["key"]
        absolute_url = f'{url}?q=London,us&appid={key}'
        # print(request.query_params['day'])
        # print(absolute_url)
        # r = requests.get(absolute_url)
        # print(r, r.status_code)

        # return Response(r.json())
        countries = SOUTH_AMERICAN_COUNTRIES_LIST
        for country in countries:
            country_name = country['name']
            country_capital = country['capital']
            country_id = country['id']
            absolute_url = f'{url}?id={country_id}&appid={key}&units=metric'
            r = requests.get(absolute_url)
            # print(r.json()['list'])
            # for temperature_list in r.json()['list']:
            country['value'] = r.json()['list']
        return Response(countries)


class CurrentLocation(APIView):
    def get(self, request, *args, **kwargs):
        url = config["OPENWEATHER"]["current"]
        key = config["OPENWEATHER"]["key"]
        lat = request.query_params['lat']
        lon = request.query_params['lon']
        absolute_url = f'{url}?lat={lat}&lon={lon}&appid={key}&units=metric'
        r = requests.get(absolute_url)
        print('i did send')
        return Response(r.json())


class CurrentLocationForecast(APIView):
    def get(self, request, *args, **kwargs):
        url = config["OPENWEATHER"]["forecast"]
        key = config["OPENWEATHER"]["key"]
        lat = request.query_params['lat']
        lon = request.query_params['lon']
        absolute_url = f'{url}?lat={lat}&lon={lon}&appid={key}&units=metric'
        r = requests.get(absolute_url)
        print('i did send')
        return Response(r.json())
