#installations

For this demo api key is added in config.ini.example
### `cp config.ini.example config.ini`

outside of project directory create venv
### ``python venv -m fincitecodingchallenge``

after activating environment
### `pip install -r requirements.txt`

### ``python fincitecodingchallenge/manage.py migrate``

### ``python fincitecodingchallenge/manage.py runserver``

##note:
Please add your database credential in config.ini file
